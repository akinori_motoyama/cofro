import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore'; // 追加
import { Observable } from 'rxjs'; // 追加

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  item: Observable<Comment>; // 追加

  constructor(db: AngularFirestore) {
    this.item = db
      .collection('Inquiries')
      .doc<Comment>('contact')
      .valueChanges();
  }

  ngOnInit() {
  }

}
