import { Component, OnInit } from '@angular/core';
// import { SwiperContainer } from './swiper/swiper.component';

declare var $;


@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {
  
  constructor() { }
  
  ngOnInit() {
    
    // var mySwiper = new Swiper('.swiper-container', {
    // });

    $(function(){
      var setImg = '#photo';
      var fadeSpeed = 1600;
      var switchDelay = 5000;
  
      $(setImg).children('img').css({opacity:'0'});
      $(setImg + ' img:first').stop().animate({opacity:'1',zIndex:'20'},fadeSpeed);
  
      setInterval(function(){
          $(setImg + ' :first-child').animate({opacity:'0'},fadeSpeed).next('img').animate({opacity:'1'},fadeSpeed).end().appendTo(setImg);
      },switchDelay);
    });
  }
}
