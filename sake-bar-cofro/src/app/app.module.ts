// モジュール
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';

// コンポーネント
import { AppComponent } from './app.component';
import { TopMenuComponent } from './top-menu/top-menu.component';
import { CompanyComponent } from './company/company.component';
import { AcsessComponent } from './acsess/acsess.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ContactComponent } from './contact/contact.component';
import { DrinkMenuComponent } from './drink-menu/drink-menu.component';
import { AboutComponent } from './about/about.component';
import { NewsComponent } from './news/news.component';

// ルーター定義
export const AppRoutes = [
  {path: "", component: TopMenuComponent},
  {path: "acsess", component: AcsessComponent},
  {path: "company", component: CompanyComponent},
  {path: "contact", component: ContactComponent},
  {path: "privacyPolicy", component: PrivacyPolicyComponent},
  {path: "DrinkMenu", component: DrinkMenuComponent},
  {path: "about", component: AboutComponent},
  {path: "news", component: NewsComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    CompanyComponent,
    TopMenuComponent,
    AcsessComponent,
    PrivacyPolicyComponent,
    ContactComponent,
    DrinkMenuComponent,
    AboutComponent,
    NewsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    RouterModule.forRoot(AppRoutes),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
